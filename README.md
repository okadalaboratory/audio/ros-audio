# ROS でAudioを使うということ

- マイクとスピーカーが別のPCにあると不便
  - スピーカーとマイクはロボットに、別のPCで重い音声処理とか
- Docker コンテナ内で音声再生は面倒
  - ホストPCの音声機能をコンテナから使う方法

## 方針

- 音声処理のコンテナでは音声の入出力は行わない
- 音声の入出力は audio_common パッケージを使う
- 音声の入出力ようにコンテナを起動する
  - このコンテナをスピーカとロボットが接続されているPCで起動する

<img src="images/audio.png" width="640">

# インストール
```
git clone https://gitlab.com/okadalaboratory/audio/ros-audio.git
```
```
make build-cpu
```
```
make run-cpu
```
```
docker-compose up
```
```
./run-docker-sound.sh
```
```
docker run -it okdhryk/audio-noetic:cpu  roslaunch docker-audio sound_play.launch
```


# audio_common
[audio_common](http://wiki.ros.org/audio_common)を使う<br>
https://github.com/ros-drivers/audio_common

- audio_capture<br>
マイクからのサウンドのキャプチャー
  - 音声ファイルに保存
  - ストリーミング（バイナリデータ）

- audio_play<br>
スピーカへのサウンドの出力

- sound_play<br>
    - 簡単な音声合成（TTS)
    - 内蔵音源の再生
    - 音声ファイル(Wav, mpg)の再生


# インストール
```
sudo apt-get install ros-noetic-audio-common
```
# audio_capture
## ノードの起動
```
$ roslaunch audio_play play.launch 
```
## テスト
そのまま出力する。<br>
ハウリングするので注意してください。<br>
```
roslaunch audio_play play.launch
```

## Pythonプログラムからサウンドを入力する
### サンプルプログラム
sample_capture.py
```
!/usr/bin/env python3
import rospy
from std_msgs.msg import String
from audio_common_msgs.msg import AudioData

def callback(data):
    rospy.loginfo(rospy.get_caller_id()+"I heard %s",data.data)
    

    
def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("/audio_capture/audio",AudioData
, callback)

    rospy.spin()
        
if __name__ == '__main__':
    listener()
```
## パラメータとノードの確認
```
PARAMETERS
 * /audio/audio_capture/bitrate: 128
 * /audio/audio_capture/channels: 1
 * /audio/audio_capture/depth: 16
 * /audio/audio_capture/device: 
 * /audio/audio_capture/dst: appsink
 * /audio/audio_capture/format: mp3
 * /audio/audio_capture/sample_format: S16LE
 * /audio/audio_capture/sample_rate: 16000
 * /rosdistro: noetic
 * /rosversion: 1.15.15

NODES
  /audio/
    audio_capture (audio_capture/audio_capture)
```


# audio_play
## ノードの起動
```
$ roslaunch audio_play play.launch 
```

## Pythonプログラムからサウンドを再生する
### サンプルプログラム
sample_audio.py
```
!/usr/bin/env python3
import rospy
from audio_common_msgs.msg import AudioData

frames = []
file = open("./input.wav", "rb")
frames=file.read()
file.close()

rospy.init_node('publisher', anonymous=True)
pub = rospy.Publisher("/audio", AudioData, queue_size=1)
while pub.get_num_connections() == 0:
    rospy.sleep(0.1)
pub.publish(frames)
rospy.sleep(5)
```

## パラメータとノードの確認
```
PARAMETERS
 * /audio/audio_play/channels: 1
 * /audio/audio_play/depth: 16
 * /audio/audio_play/device: 
 * /audio/audio_play/do_timestamp: False
 * /audio/audio_play/dst: alsasink
 * /audio/audio_play/format: mp3
 * /audio/audio_play/sample_format: S16LE
 * /audio/audio_play/sample_rate: 16000
 * /rosdistro: noetic
 * /rosversion: 1.15.15

NODES
  /audio/
    audio_play (audio_play/audio_play)

```


# sound_play
[【ROS】音声再生パッケージsound_playを使ってみた](https://www.sato-susumu.com/entry/sound_play)

##　ノードの起動
```
$ roslaunch sound_play soundplay_node.launch
```
## テスト
別のターミナルを開き、下記のコマンドを実行するとテストが実行されます。
```
$ rosrun sound_play test.py
[INFO] [1668408042.225281]: This script will run continuously until you hit CTRL+C, testing various sound_node sound types.
[INFO] [1668408042.227821]: wave
[INFO] [1668408044.250219]: quiet wave
[INFO] [1668408046.274122]: plugging
[INFO] [1668408048.277964]: quiet plugging
[INFO] [1668408050.282618]: unplugging
[INFO] [1668408052.285954]: plugging badly
[INFO] [1668408054.289835]: unplugging badly
[INFO] [1668408056.334298]: New API start voice
[INFO] [1668408059.337887]: New API start voice quiet
[INFO] [1668408062.344257]: New API wave
[INFO] [1668408064.348993]: New API wave quiet
[INFO] [1668408066.354983]: New API builtin
[INFO] [1668408068.359735]: New API builtin quiet
[INFO] [1668408070.365346]: New API stop
```
## 音声ファイルを再生してみる
WAV形式やOGG形式の音声ファイルを再生できます。
```
$ rosrun sound_play play.py --help
$ rosrun sound_play play.py /opt/ros/noetic/share/sound_play/sounds/say-beep.wav
$ rosrun sound_play play.py /opt/ros/noetic/share/sound_play/sounds/BACKINGUP.ogg
```
## 内蔵サウンドを再生してみる
```
$ rosrun sound_play playbuiltin.py --help
$ rosrun sound_play playbuiltin.py 2
```
## 組み込み音声の一覧を表示する
```
$ cat `rospack find sound_play`/msg/SoundRequest.msg
```

## 音声合成（TTS）を試してみる
英語のみ
```
$ rosrun sound_play say.py --help  $ rosrun sound_play say.py 'Hello world'  
$ echo Hello again | rosrun sound_play say.py
```

## Pythonプログラムからサウンドを再生する
### サンプルプログラム
sample_sound.py
```
#!/usr/bin/env python3
import sys
import rospy
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

rospy.init_node('sample_sound', anonymous=True)
soundhandle = SoundClient()
rospy.sleep(1)

voice = 'voice_kal_diphone'
volume = 1.0  #ボリューム (0:ミュート、1.0:最大）

# 音声ファイルを再生する
soundhandle.playWave('/opt/ros/noetic/share/sound_play/sounds/say-beep.wav', volume)
rospy.sleep(2)

# 内蔵音声を再生する
soundhandle.play(3, volume)
rospy.sleep(2)

# 音声合成を試す
soundhandle.say("Hello World!", voice, volume)
rospy.sleep(2)
```
### 実行方法
sound_playノードを起動します。
```
$ roslaunch sound_play soundplay_node.launch
```
別のターミナルを開き、Pythonプログラムを実行して下さい。
```
$ rosrun ./sample_sound.py
```

## パラメータとノードの確認
```
PARAMETERS
 * /rosdistro: noetic
 * /rosversion: 1.15.15
 * /soundplay_node/default_voice: 
 * /soundplay_node/device: 
 * /soundplay_node/loop_rate: 100
 * /soundplay_node/plugin: sound_play/festiv...

NODES
  /
    soundplay_node (sound_play/soundplay_node.py)
  /sound_play/
    is_speaking (sound_play/is_speaking.py)


```

