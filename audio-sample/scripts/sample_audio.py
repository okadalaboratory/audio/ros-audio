#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

import rospy
from audio_common_msgs.msg import AudioData

frames = []
file = open("./sample.wav", "rb")
frames=file.read()
file.close()

rospy.init_node('publisher', anonymous=True)
pub = rospy.Publisher("/audio", AudioData, queue_size=1)
while pub.get_num_connections() == 0:
    rospy.sleep(0.1)
pub.publish(frames)
rospy.sleep(5)
