#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php


import sys
import rospy
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

rospy.init_node('sample_sound', anonymous=True)
soundhandle = SoundClient()
rospy.sleep(1)

voice = 'voice_kal_diphone'
volume = 1.0  #ボリューム (0:ミュート、1.0:最大）

# 音声ファイルを再生する
soundhandle.playWave('/opt/ros/noetic/share/sound_play/sounds/say-beep.wav', volume)
rospy.sleep(2)

# 内蔵音声を再生する
soundhandle.play(3, volume)
rospy.sleep(2)

# 音声合成を試す
soundhandle.say("Hello World!", voice, volume)
rospy.sleep(2)

