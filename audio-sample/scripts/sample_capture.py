#!/usr/bin/env python3
## coding: UTF-8
# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

import rospy
from std_msgs.msg import String
from audio_common_msgs.msg import AudioData

def callback(data):
    rospy.loginfo(rospy.get_caller_id()+"I heard %s",data.data)
    
    
def listener():
    rospy.init_node('listener', anonymous=True)
    rospy.Subscriber("/audio_capture/audio",AudioData
, callback)

    rospy.spin()
        
if __name__ == '__main__':
    listener()
