# Copyright (c) 2023 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

DOCKER_USER = okdhryk/
ENV = --env="ROS_IP=192.168.195.20"  --env="ROS_MASTER_URI=192.168.195.80:11311"


# If the first argument is ...
ifneq (,$(findstring tools_,$(firstword $(MAKECMDGOALS))))
	# use the rest as arguments
	RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
	# ...and turn them into do-nothing targets
	#$(eval $(RUN_ARGS):;@:)
endif

.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[\.0-9a-zA-Z_-]+:.*?## / {printf "\033[36m%-42s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

build-cpu: ## Build VoiceVox [CPU] Container
	docker build --target audio-cpu -t okdhryk/audio-noetic:cpu -f docker/Dockerfile .
	@printf "\n\033[92mBuild Docker Image: okdhryk/audio-noetic:cpu\033[0m\n"


restart: stop run

run-cpu: ## RUN VoiceVox [CPU] Container
	pacmd load-module module-native-protocol-unix socket=/tmp/pulseaudio.socket;docker run --rm -it --privileged --net=host --ipc=host \
	--device=/dev/snd:/dev/snd \
	--env PULSE_SERVER=unix:/tmp/pulseaudio.socket \
	--env PULSE_COOKIE=/tmp/pulseaudio.cookie \
	--volume /tmp/pulseaudio.socket:/tmp/pulseaudio.socket \
	$(DOCKER_USER)audio-noetic:cpu
	@printf "\n\033[92mRun Docker Image: $(DOCKER_USER)audio-noetic:cpu\033[0m\n"


up: ## docker-compose up VoiceVox [GPU] Base Container
	docker-compose up

contener=`docker ps -a -q`
image=`docker images | awk '/^<none>/ { print $$3 }'`

stop: ## STOP All Container
	docker rm -f $(contener)

attach: ## ATTACH Noetic [CPU] Base Container
	docker exec -it $(NAME) /bin/bash

logs: ## Logs Noetic [CPU] Base Container
	docker logs $(NAME)



