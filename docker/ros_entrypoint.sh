#!/bin/bash
set -e
# Copyright (c) 2022 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

source /opt/ros/noetic/setup.bash
source /overlay_ws/devel/setup.bash

if which "$1" > /dev/null 2>&1 ; then
	$EXEC "$@"
else
	echo $@ | $EXEC $SHELL -li
fi
