#!/bin/bash
set -e
# Copyright (c) 2022 Hiroyuki Okada
# This software is released under the MIT License.
# http://opensource.org/licenses/mit-license.php

# CPU mode
echo !!!!! Docker Sound CPU mode !!!!!
# Download docker image from DockerHub
echo Download docker image from DockerHub
docker pull okdhryk/audio-noetic:cpu

#
pacmd load-module module-native-protocol-unix socket=/tmp/pulseaudio.socket;docker-compose -f docker-compose.yml up -d


